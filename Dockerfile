FROM amazoncorretto:11

LABEL maintainer="yudhkra212@gmail.com"

WORKDIR /app

COPY target/file-management-0.0.1-SNAPSHOT.jar /app/springboot-docker-app.jar

ENTRYPOINT ["java", "-jar", "springboot-docker-app.jar"]
