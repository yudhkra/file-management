package com.file.management.utils;

import lombok.AllArgsConstructor;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDownloadUtil {

    private Path foundFile;
    public Resource getFileAsResource(String fileCode) throws IOException {

        //Validate file path
        if (FileUploadUtil.getUploadPath() == null) {
            return null;
        }

        //Get file path
        Path uploadDirectory = Paths.get(FileUploadUtil.getUploadPath());

        //Search file
        Files.list(uploadDirectory).forEach(file -> {
            if (file.getFileName().toString().startsWith(fileCode)) {
                foundFile = file;
                return;
            }
        });

        if (foundFile != null) {
            return new UrlResource(foundFile.toUri());
        }

        return null;
    }
}
