package com.file.management.controller;

import com.file.management.utils.FileDownloadUtil;
import com.file.management.utils.FileUploadResponse;
import com.file.management.utils.FileUploadUtil;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FilesController {

    @PostMapping("/uploadFile")
    public ResponseEntity<?> uploadFile(
            @RequestPart("file") MultipartFile multipartFile) {

        Long size = multipartFile.getSize();

        //Validate file size
        if (size > 10000000) {
            return new ResponseEntity<>("Error: File harus kurang dari 10MB", HttpStatus.BAD_REQUEST);
        }

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());


        try {
            String fileCode = FileUploadUtil.saveFile(fileName, multipartFile);
            FileUploadResponse response = new FileUploadResponse();

            response.setFileName(fileName);
            response.setFileCode(fileCode);
            response.setSize(size);
            response.setDownloadUri("/downloadFile/" + fileCode);

            return new ResponseEntity<FileUploadResponse>(response, HttpStatus.OK);
        } catch (IOException e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/downloadFile/{fileCode}")
    public ResponseEntity<?> downloadFile(@PathVariable("fileCode") String fileCode) {

        FileDownloadUtil downloadUtil = new FileDownloadUtil();
        Resource fileAsResource = null;

        try {
            fileAsResource = downloadUtil.getFileAsResource(fileCode);
        } catch (IOException e) {
            return ResponseEntity.internalServerError().build();
        }

        if (fileAsResource == null) {
            return new ResponseEntity<>("File not found", HttpStatus.NOT_FOUND);
        }

        String contentType = "application/octet-stream";
        String headerValue = "attachment; filename=\"" + fileAsResource.getFilename() + "\"";

        return ResponseEntity.ok().
                contentType(MediaType.parseMediaType(contentType)).
                header(HttpHeaders.CONTENT_DISPOSITION, headerValue).
                body(fileAsResource);
    }
}
